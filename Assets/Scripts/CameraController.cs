﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    [SerializeField]
    private Transform _trackObject;
    private float _baseYPos;
    private float _baseZPos;

    private void Start() {
        _baseYPos = transform.position.y;
        _baseZPos = transform.position.z;
    }

    private void Update() {
        transform.position = new Vector3(_trackObject.position.x, _baseYPos, _baseZPos);
    }
}
