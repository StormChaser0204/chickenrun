﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityStandardAssets.CrossPlatformInput;

public class CharacterController : MonoBehaviour {
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private Rigidbody _rb;
    [SerializeField]
    private Transform _tr;
    [SerializeField]
    private float _moveForce;
    [SerializeField]
    private float _jumpForce;
    [SerializeField]
    private float _dashForce;

    private bool _onGround;
    private bool _moveRight;
    private void Update() {
        MakeMove();
        if (CrossPlatformInputManager.GetButtonDown("Jump")) {
            MakeJump();
        }
        if (CrossPlatformInputManager.GetButtonDown("Dash")) {
            MakeDash();
        }
    }

    private void MakeMove() {
        var axisValue = CrossPlatformInputManager.GetAxis("Horizontal");
        if (axisValue != 0) {
            var newMoveRight = axisValue > 0;
            if (_moveRight != newMoveRight) {
                MakeRotate();
                _moveRight = newMoveRight;
            }
            _rb.MovePosition(transform.position + axisValue * transform.forward * _moveForce * Time.fixedDeltaTime);
            _animator.SetBool("Run", true);
        }
        else
            _animator.SetBool("Run", false);
    }

    public void MakeJump() {
        if (!_onGround) return;
        _animator.SetTrigger("Jump");
        _animator.SetBool("OnGround", false);
        _rb.AddForce(Vector3.up * _jumpForce);
        _onGround = false;
    }

    public void MakeDash() {
        var dashDirection = _moveRight ? -1 : 1;
        _rb.AddRelativeForce(dashDirection * Vector3.right * _dashForce);
        _animator.SetTrigger("Dash");
    }

    public void MakeRotate() {
        _tr.DORotate(new Vector3(0, _moveRight ? 90 : 270, 0), 0.2f);
    }

    private void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag == "Ground") {
            _onGround = true;
            _animator.SetBool("OnGround", true);
        }
    }
}
